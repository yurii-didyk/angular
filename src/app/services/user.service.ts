import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private client: HttpClient) { }

  getUsers(): Observable<User[]> {
    return this.client.get<User[]>('http://localhost:50351/api/users');
  }

  postUser(user: User): Observable<User> {
    return this.client.post<User>('http://localhost:50351/api/users', user);
  }

  deleteUser(id: number) {
    return this.client.delete(`http://localhost:50351/api/users/${id}`);
  }

  updateUser(id: number, newUser: User) {
    return this.client.put<User>(`http://localhost:50351/api/users/${id}`, newUser);
  }
}