import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import {Project} from '../models/project';

@Injectable({
  providedIn: 'root'
 })
export class ProjectService {

  constructor(private client: HttpClient) { }

  getProjects(): Observable<Project[]> {
    return this.client.get<Project[]>('http://localhost:50351/api/projects');
  }

  postProject(project: Project): Observable<Project> {
    return this.client.post<Project>('http://localhost:50351/api/projects', project);
  }

  deleteProject(id: number) {
    return this.client.delete(`http://localhost:50351/api/projects/${id}`);
  }

  updateProject(id: number, newProject: Project): Observable<Project> {
    return this.client.put<Project>(`http://localhost:50351/api/projects/${id}`, newProject);
  }
}
