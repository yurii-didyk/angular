import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Task } from '../models/task';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  constructor(private client: HttpClient) { }

  getTasks(): Observable<Task[]> {
    return this.client.get<Task[]>('http://localhost:50351/api/tasks');
  }

  postTask(task: Task): Observable<Task> {
    return this.client.post<Task>('http://localhost:50351/api/tasks', task);
  }

  deleteTask(id: number) {
    return this.client.delete(`http://localhost:50351/api/tasks/${id}`);
  }

  updateTask(id: number, newTask: Task) {
    return this.client.put<Task>(`http://localhost:50351/api/tasks/${id}`, newTask);
  }
}
