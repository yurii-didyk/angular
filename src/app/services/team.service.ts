import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Team } from '../models/team';

@Injectable({
  providedIn: 'root'
})
export class TeamService {

  constructor(private client: HttpClient) { }

  getTeams(): Observable<Team[]> {
    return this.client.get<Team[]>('http://localhost:50351/api/teams');
  }

  postTeam(team: Team): Observable<Team> {
    return this.client.post<Team>('http://localhost:50351/api/teams', team);
  }

  deleteTeam(id: number) {
    return this.client.delete(`http://localhost:50351/api/teams/${id}`);
  }

  updateTeam(id: number, newTeam: Team) {
    return this.client.put<Team>(`http://localhost:50351/api/teams/${id}`, newTeam);
  }
}