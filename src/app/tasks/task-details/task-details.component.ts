import { Component, OnInit, OnDestroy, OnChanges, Input, Output, EventEmitter, SimpleChanges} from '@angular/core';
import { Project } from 'src/app/models/project';
import { UserService } from 'src/app/services/user.service';
import { Task } from 'src/app/models/task';
import { TaskService } from 'src/app/services/task.service';
import { TasksListComponent } from '../tasks-list/tasks-list.component';
import { TasksPageComponent } from '../tasks-page/tasks-page.component';
import { Observable } from 'rxjs';
import { ProjectService } from 'src/app/services/project.service';

@Component({
  selector: 'task-details',
  templateUrl: './task-details.component.html',
  styleUrls: ['./task-details.component.css']
})
export class TaskDetailsComponent implements OnInit, OnDestroy, OnChanges {

  @Input() item: Task;
  project: Project;
  state: string;
  @Input('index') itemIndex: number;
  @Output() itemSelected = new EventEmitter<number>();

  constructor(private taskService: TaskService,
    private taskPage: TasksPageComponent, private projectService: ProjectService) { 
  }

  ngOnInit() {
    this.projectService.getProjects().subscribe(x => {
      x.forEach(element => {
        if (element.id === this.item.project_id) {
          this.project = element;
        }
      });
    })
    console.log('TaskDetailsComponent init')
    switch(this.item.state)
    {
      case 0:
        this.state = 'Created';
        break;
      case 1:
        this.state = 'Started'
        break;
      case 2:
        this.state = 'Finished';
        break;
      case 3:
        this.state = 'Canceled';
        break;
    }
  }
  ngOnChanges(changes: SimpleChanges): void {
    console.log('TaskDetailsComponent ngOnChanges', changes)
}

  taskSelected() {
    this.itemSelected.emit(this.itemIndex);
  }

  ngOnDestroy(){
    console.log('TaskDetailsComponent ngOnDestroy')
  }


  deleteTask() {
      console.log('delete');
      this.taskService.deleteTask(this.item.id).subscribe(x => {
        this.taskPage.getTasks();
      });
  }

}
