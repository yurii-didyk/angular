import { Component, OnInit, OnChanges, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Project} from "../../models/project";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TaskService } from 'src/app/services/task.service';
import { Task } from 'src/app/models/task';
import { Observable } from 'rxjs';
import { TasksPageComponent } from '../tasks-page/tasks-page.component';
import { DialogService } from 'src/app/services/dialog.service';


@Component({
  selector: 'app-task-create',
  templateUrl: './task-create.component.html',
  styleUrls: ['./task-create.component.css'],
  providers: [TaskService]

})
export class TaskCreateComponent implements OnInit, OnChanges {
  
  @Input() task: Task = {} as Task;
  @Output() taskChange = new EventEmitter<Task>();
  taskForm: FormGroup;

  constructor(private taskService: TaskService, private taskPage: TasksPageComponent,
    private dialogService: DialogService) { }


  ngOnInit() {
    this.taskForm = new FormGroup({
      'name': new FormControl(this.task.name, [
          Validators.required,
      ]),
      'description': new FormControl(this.task.description, [
        Validators.required,
        Validators.maxLength(250)
      ]),
      'state': new FormControl(this.task.state,                [
          Validators.required,
      ]),
      'project_id': new FormControl(this.task.project_id, [
        Validators.required
      ]),
      'performer_id': new FormControl(this.task.performer_id,[
          Validators.required
      ])
  });
  }

  ngOnChanges(changes: SimpleChanges){
    console.log(changes)
    if(changes.task && this.taskForm){
        this.taskForm.setValue(changes.task.currentValue)
    }
  }

  canDeactivate(): Observable<boolean> | boolean {

    if (!this.taskPage.hideCreateTask && this.taskForm.dirty) {

        return this.dialogService.confirm('Are u sure?');
    }
    return true;
  }	

  saveTask() {
    console.log('Emit event');
    let newTask = this.taskForm.value
    newTask.created_at = new Date(Date.now()).toJSON();
    if (newTask.state === 2) {
      newTask.finished_at = new Date(Date.now()).toJSON();
    }
    console.log(newTask);
    this.taskChange.emit(newTask);
    this.taskForm.reset();
}

  revert(){
    this.taskForm.reset();
  }

}
