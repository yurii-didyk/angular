import { Component, OnInit } from '@angular/core';
import { TaskService } from 'src/app/services/task.service';
import {Task } from 'src/app/models/task';
import { map } from 'rxjs/operators';
import { TasksListComponent } from '../tasks-list/tasks-list.component';
import { Observable } from 'rxjs';
import { DialogService } from 'src/app/services/dialog.service';

@Component({
  selector: 'app-tasks-page',
  templateUrl: './tasks-page.component.html',
  styleUrls: ['./tasks-page.component.css'],
  providers: [TaskService]
})
export class TasksPageComponent implements OnInit {

  
	hideCreateTask = true;
  tasks: Task[] = [];
  selectedTask: Task = {} as Task;
  selectedTaskIndex: number = -1;
  constructor(private taskService: TaskService, private dialogService: DialogService) { }

  ngOnInit() {
    this.getTasks();
  }

  getTasks() {
    this.tasks = [];
    this.taskService.getTasks().pipe(map(x => {
      x = x.map(b => {
          b.created_at = new Date(b.created_at);
          b.finished_at = new Date(b.finished_at);
          return b;
      })
      return x;
  })).subscribe(x => {
      console.log(x);
      x.forEach(element => {
        this.tasks.push(element);
      });
    }, error => {
      console.log(error);
    })
  }

  showEdit() {
    if (this.hideCreateTask) {
      this.selectedTaskIndex = -1;
    } 
    this.selectedTask = {
      name: '',
      description: '',
      state: -1,
      project_id: null,
      performer_id: null
    } as Task;
    this.hideCreateTask = !this.hideCreateTask;
  }

  canDeactivate() : boolean | Observable<boolean>{
     
    if(!this.hideCreateTask){
        return this.dialogService.confirm("Are u sure?");
    }
    else{
        return true;
    }
  }

  save(newTask: Task){
    newTask.created_at = new Date(Date.now());
    if (newTask.state === 2) {
      newTask.finished_at = new Date(Date.now());
    }
    if(this.selectedTaskIndex === -1){
        this.tasks.push(newTask);
        this.taskService.postTask(newTask).subscribe(x => {
          console.log(x);
        });
    } else {
        newTask.id = this.tasks[this.selectedTaskIndex].id;
        this.tasks[this.selectedTaskIndex] = newTask;
        this.taskService.updateTask(this.tasks[this.selectedTaskIndex].id, newTask).subscribe(x => {
          console.log(x);
        });
    }
    this.selectedTaskIndex = -1;
    this.hideCreateTask = true;
}

select(index: number) {
    let task = this.tasks[index];
    this.selectedTask = {
        name: task.name,
        description: task.description,
        state: task.state,
        project_id: task.project_id,
        performer_id: task.performer_id

    } as Task;
    this.selectedTaskIndex = index;
    this.hideCreateTask = false;
    console.log(this.selectedTask)
}

}
