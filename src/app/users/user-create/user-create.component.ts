import { Component, OnInit, OnChanges, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Project} from "../../models/project";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/models/user';


@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.css'],
  providers: [UserService]

})
export class UserCreateComponent implements OnInit, OnChanges {
  
  @Input() user: User = {} as User;
  @Output() userChange = new EventEmitter<User>();
  userForm: FormGroup;

  constructor(private userService: UserService) { }


  ngOnInit() {
    this.userForm = new FormGroup({
      'first_name': new FormControl(this.user.first_name, [
          Validators.required,
      ]),
      'last_name': new FormControl(this.user.last_name, [
        Validators.required,
      ]),
      'email': new FormControl(this.user.email, [
        Validators.required,
        Validators.email
      ]),
      'birthday': new FormControl(this.user.birthday, [
        Validators.required,
      ]),
      'team_id': new FormControl(this.user.team_id, [
      ])
  });
  }

  ngOnChanges(changes: SimpleChanges){
    console.log(changes)
    if(changes.user && this.userForm){
        this.userForm.setValue(changes.user.currentValue)
    }
  }

  saveUser() {
    console.log('Emit event');
    let newUser = this.userForm.value
    newUser.registered_at = new Date(Date.now()).toJSON();
    console.log(newUser);
    this.userChange.emit(newUser);
    this.userForm.reset();
}

  revert(){
    this.userForm.reset();
  }

}
