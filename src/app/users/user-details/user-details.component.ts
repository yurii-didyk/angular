import { Component, OnInit, OnDestroy, OnChanges, Input, Output, EventEmitter, SimpleChanges} from '@angular/core';
import { Project } from 'src/app/models/project';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/models/user';
import { UsersListComponent } from '../users-list/users-list.component';
import { UsersPageComponent } from '../users-page/users-page.component';

@Component({
  selector: 'user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit, OnDestroy, OnChanges {

  @Input() item: User;
  @Input('index') itemIndex: number;
  @Output() itemSelected = new EventEmitter<number>();

  constructor(private userService: UserService,
    private userPage: UsersPageComponent) { }

  ngOnInit() {
    console.log('TeamDetailsComponent init')
  }
  ngOnChanges(changes: SimpleChanges): void {
    console.log('TeamDetailsComponent ngOnChanges', changes)
}

  userSelected() {
    this.itemSelected.emit(this.itemIndex);
  }

  ngOnDestroy(){
    console.log('TeamDetailsComponent ngOnDestroy')
  }
  deleteUser() {
    console.log('delete');
    this.userService.deleteUser(this.item.id).subscribe(x => {
      this.userPage.getUsers();
    });
  }

}
