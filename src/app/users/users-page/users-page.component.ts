import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import {User } from 'src/app/models/user';
import { map } from 'rxjs/operators';
import { DialogService } from 'src/app/services/dialog.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-users-page',
  templateUrl: './users-page.component.html',
  styleUrls: ['./users-page.component.css'],
  providers: [UserService]
})
export class UsersPageComponent implements OnInit {

  
	hideCreateUser = true;
  users: User[] = [];
  selectedUser: User = {} as User;
  selectedUserIndex: number = -1;
  constructor(private userService: UserService, private dialogService: DialogService) { }

  ngOnInit() {
    this.getUsers();
  }

  getUsers() {
    this.users = [];
    this.userService.getUsers().pipe(map(x => {
      x = x.map(b => {
          b.birthday = new Date(b.birthday);
          b.registered_at = new Date(b.registered_at);
          return b;
      })
      return x;
  })).subscribe(x => {
      console.log(x);
      x.forEach(element => {
        this.users.push(element);
      });
    }, error => {
      console.log(error);
    })
  }

  showEdit() {
    if (this.hideCreateUser) {
      this.selectedUserIndex = -1;
    } 
    this.selectedUser = {
      first_name: '',
      last_name: '',
      birthday: new Date(),
      email: '',
      team_id: null
    } as User;
    this.hideCreateUser = !this.hideCreateUser;
  }
  canDeactivate() : boolean | Observable<boolean>{
     
    if(!this.hideCreateUser){
        return this.dialogService.confirm("Are u sure?");
    }
    else{
        return true;
    }
  }

  save(newUser: User) {
    newUser.registered_at = new Date(Date.now());
    if(this.selectedUserIndex === -1){
        this.users.push(newUser);
        this.userService.postUser(newUser).subscribe(x => {
          console.log(x);
        });
    } else {
        newUser.id = this.users[this.selectedUserIndex].id;
        this.users[this.selectedUserIndex] = newUser;
        this.userService.updateUser(this.users[this.selectedUserIndex].id, newUser).subscribe(x => {
          console.log(x);
        });
        
    }
    this.selectedUserIndex = -1;
    this.hideCreateUser = true;
}

select(index: number) {
    let user = this.users[index];
    this.selectedUser = {
        first_name: user.first_name,
        last_name: user.last_name,
        birthday: user.birthday,
        email: user.email,
        team_id: user.team_id
    } as User;
    this.selectedUserIndex = index;
    this.hideCreateUser = false;
    console.log(this.selectedUser)
}

}