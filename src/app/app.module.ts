import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app-root/app/app.component';
import { ProjectCreateComponent } from './project/project-create/project-create.component';
import { ProjectDetailsComponent } from './project/project-details/project-details.component';
import { ProjectsListComponent } from './project/projects-list/projects-list.component';
import { ProjectsPageComponent } from './project/projects-page/projects-page.component';
import { TaskCreateComponent } from './tasks/task-create/task-create.component';
import { TaskDetailsComponent } from './tasks/task-details/task-details.component';
import { TasksListComponent } from './tasks/tasks-list/tasks-list.component';
import { TasksPageComponent } from './tasks/tasks-page/tasks-page.component';
import { TeamCreateComponent } from './teams/team-create/team-create.component';
import { TeamDetailsComponent } from './teams/team-details/team-details.component';
import { TeamsListComponent } from './teams/teams-list/teams-list.component';
import { TeamsPageComponent } from './teams/teams-page/teams-page.component';
import { UserCreateComponent } from './users/user-create/user-create.component';
import { UserDetailsComponent } from './users/user-details/user-details.component';
import { UsersListComponent } from './users/users-list/users-list.component';
import { UsersPageComponent } from './users/users-page/users-page.component';
import { NavigationMenuComponent } from './navigation-menu/navigation-menu.component';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { DatePipe } from './pipes/date.pipe';
import { registerLocaleData } from '@angular/common';
import localeUA from '@angular/common/locales/uk';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { StateDirective } from './directives/state.directive';
import { CanDeactivateGuard } from './guards/can-leave-form.guard';

registerLocaleData(localeUA, 'ua');

@NgModule({
  declarations: [
    AppComponent,
    ProjectCreateComponent,
    ProjectDetailsComponent,
    ProjectsListComponent,
    ProjectsPageComponent,
    TaskCreateComponent,
    TaskDetailsComponent,
    TasksListComponent,
    TasksPageComponent,
    TeamCreateComponent,
    TeamDetailsComponent,
    TeamsListComponent,
    TeamsPageComponent,
    UserCreateComponent,
    UserDetailsComponent,
    UsersListComponent,
    UsersPageComponent,
    NavigationMenuComponent,
    DatePipe,
    StateDirective
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
  	AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFontAwesomeModule
  ],
  providers: [CanDeactivateGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
