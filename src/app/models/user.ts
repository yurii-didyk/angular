
export interface User {
    id?: number,
    first_name: string,
    last_name: string,
    registered_at: Date,
    email: string,
    birthday: Date,
    team_id: number
}