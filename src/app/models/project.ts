export interface Project {
    id?: number,
    name: string,
    description: string,
    created_at: Date,
    deadline: Date,
    author_id: number,
    team_id: number
}