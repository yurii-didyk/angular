export interface Task {
    id?: number,
    name: string,
    description: string, 
    created_at: Date,
    finished_at: Date,
    state: number,
    project_id: number,
    performer_id: number
}