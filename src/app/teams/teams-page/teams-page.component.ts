import { Component, OnInit } from '@angular/core';
import { TeamService } from 'src/app/services/team.service';
import {Team } from 'src/app/models/Team';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { DialogService } from 'src/app/services/dialog.service';

@Component({
  selector: 'app-teams-page',
  templateUrl: './teams-page.component.html',
  styleUrls: ['./teams-page.component.css'],
  providers: [TeamService]
})
export class TeamsPageComponent implements OnInit {

  
	hideCreateTeam = true;
  teams: Team[] = [];
  selectedTeam: Team = {} as Team;
  selectedTeamIndex: number = -1;
  constructor(private teamService: TeamService, private dialogService: DialogService) { }

  ngOnInit() {
    this.getTeams();
  }

  getTeams() {
    this.teams = [];
    this.teamService.getTeams().pipe(map(x => {
      x = x.map(b => {
          b.created_at = new Date(b.created_at);
          return b;
      })
      return x;
  })).subscribe(x => {
      console.log(x);
      x.forEach(element => {
        this.teams.push(element);
      });
    }, error => {
      console.log(error);
    })
  }

  showEdit() {
    if (this.hideCreateTeam) {
      this.selectedTeamIndex = -1;
    } 
    this.selectedTeam = {
      name: '',
    } as Team;
    this.hideCreateTeam = !this.hideCreateTeam;
  }

  save(newTeam: Team){
    newTeam.created_at = new Date(Date.now());
    if(this.selectedTeamIndex === -1){
        this.teams.push(newTeam);
        this.teamService.postTeam(newTeam).subscribe(x => {
          console.log(x);
        });
    } else {
      newTeam.id = this.teams[this.selectedTeamIndex].id;
      this.teams[this.selectedTeamIndex] = newTeam;
        this.teamService.updateTeam(this.teams[this.selectedTeamIndex].id, newTeam).subscribe(x => {
          console.log(x);
        });
        
    }
    this.selectedTeamIndex = -1;
    this.hideCreateTeam = true;
}
canDeactivate() : boolean | Observable<boolean>{
     
  if(!this.hideCreateTeam){
      return this.dialogService.confirm("Are u sure?");
  }
  else{
      return true;
  }
}

select(index: number) {
    let team = this.teams[index];
    this.selectedTeam = {
        name: team.name,
    } as Team;
    this.selectedTeamIndex = index;
    this.hideCreateTeam = false;
    console.log(this.selectedTeam)
}

}