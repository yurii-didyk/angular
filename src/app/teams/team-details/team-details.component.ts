import { Component, OnInit, OnDestroy, OnChanges, Input, Output, EventEmitter, SimpleChanges} from '@angular/core';
import { Project } from 'src/app/models/project';
import { UserService } from 'src/app/services/user.service';
import { Team } from 'src/app/models/team';
import { TeamService } from 'src/app/services/team.service';
import { TeamsListComponent } from '../teams-list/teams-list.component';
import { TeamsPageComponent } from '../teams-page/teams-page.component';
import { UsersListComponent } from 'src/app/users/users-list/users-list.component';

@Component({
  selector: 'team-details',
  templateUrl: './team-details.component.html',
  styleUrls: ['./team-details.component.css']
})
export class TeamDetailsComponent implements OnInit, OnDestroy, OnChanges {

  @Input() item: Team;
  users: string[] = [];
  @Input('index') itemIndex: number;
  @Output() itemSelected = new EventEmitter<number>();

  constructor(private teamService: TeamService,
    private teamPage: TeamsPageComponent, private userService: UserService) { }

  ngOnInit() {
    console.log('TeamDetailsComponent init')
    this.userService.getUsers().subscribe(x => {
      x.forEach(element => {
        if (element.team_id === this.item.id){
          this.users.push(`${element.first_name} ${element.last_name}`);
        }
      });
    })
  }
  ngOnChanges(changes: SimpleChanges): void {
    console.log('TeamDetailsComponent ngOnChanges', changes)
}

  teamSelected() {
    this.itemSelected.emit(this.itemIndex);
  }

  ngOnDestroy(){
    console.log('TeamDetailsComponent ngOnDestroy')
  }


  deleteTeam() {
    console.log('delete');
    this.teamService.deleteTeam(this.item.id).subscribe(x => {
      this.teamPage.getTeams();
    });
  }

}