import { TestBed, async, inject } from '@angular/core/testing';

import { CanLeaveFormGuard } from './can-leave-form.guard';

describe('CanLeaveFormGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CanLeaveFormGuard]
    });
  });

  it('should ...', inject([CanLeaveFormGuard], (guard: CanLeaveFormGuard) => {
    expect(guard).toBeTruthy();
  }));
});
