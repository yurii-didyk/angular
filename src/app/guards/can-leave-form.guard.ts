import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { CanDeactivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { ProjectCreateComponent } from '../project/project-create/project-create.component';
import { DialogService } from '../services/dialog.service';
import { ProjectsPageComponent } from '../project/projects-page/projects-page.component';

export interface ComponentCanDeactivate{
  canDeactivate: () => boolean | Observable<boolean>;
}

@Injectable()
export class CanDeactivateGuard implements CanDeactivate<ComponentCanDeactivate> {
  
   constructor(private dialogService: DialogService,/* private projectPage: ProjectsPageComponent*/) {}
  canDeactivate(component: ComponentCanDeactivate, 
           route: ActivatedRouteSnapshot, 
           state: RouterStateSnapshot) {
     let url: string = state.url;
     console.log('Url: '+ url);
     return component.canDeactivate ? component.canDeactivate() : true;
  }
} 