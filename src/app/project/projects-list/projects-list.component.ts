import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { Project } from 'src/app/models/project';

@Component({
  selector: 'projects-list',
  templateUrl: './projects-list.component.html',
  styleUrls: ['./projects-list.component.css']
})
export class ProjectsListComponent implements OnInit {

  constructor() { }

  title = 'Projects';

  @Input('projects') projectsList: Project[];
  @Output() itemSelected =  new EventEmitter<number>();

	ngOnInit() {
    }
    
    projectSelected(index: number){
        this.itemSelected.emit(index);
    }

}
