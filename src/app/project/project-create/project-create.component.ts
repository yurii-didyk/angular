import { Component, OnInit, OnChanges, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Project} from "../../models/project";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProjectService } from 'src/app/services/project.service';
import { TeamService } from 'src/app/services/team.service';
import { UserService } from 'src/app/services/user.service';
import { DialogService } from 'src/app/services/dialog.service';
import { Observable } from 'rxjs';
import { ProjectsPageComponent } from '../projects-page/projects-page.component';
import { ComponentCanDeactivate, CanDeactivateGuard } from 'src/app/guards/can-leave-form.guard';


@Component({
  selector: 'app-project-create',
  templateUrl: './project-create.component.html',
  styleUrls: ['./project-create.component.css'],
  providers: [ProjectService]

})
export class ProjectCreateComponent implements ComponentCanDeactivate, OnInit, OnChanges {
  
  @Input() project: Project = {} as Project;
  @Output() projectChange = new EventEmitter<Project>();
  userIds: number[];
  projectForm: FormGroup;
  

  constructor(private projectService: ProjectService, private userService: UserService, private dialogService: DialogService,
    private projectPage: ProjectsPageComponent) { }


  ngOnInit() {
    this.projectForm = new FormGroup({
      'name': new FormControl(this.project.name, [
          Validators.required,
      ]),
      'description': new FormControl(this.project.description, [
        Validators.required,
        Validators.maxLength(250)
      ]),
      'deadline': new FormControl(this.project.deadline,                [
          Validators.required,
      ]),
      'team_id': new FormControl(this.project.team_id, [
        Validators.required
      ]),
      'author_id': new FormControl(this.project.author_id,[
          Validators.required
      ])
  });
  }

  ngOnChanges(changes: SimpleChanges){
    console.log(changes)
    if(changes.project && this.projectForm){
        this.projectForm.setValue(changes.project.currentValue)
    }
  }

  saveProject() {
    console.log('Emit event');
    let newProject = this.projectForm.value
    newProject.created_at = new Date(Date.now()).toJSON();
    console.log(newProject);
    this.projectChange.emit(newProject);
    this.projectForm.reset();
}

  revert(){
    this.projectForm.reset();
  }
  canDeactivate() : boolean | Observable<boolean>{
     
    if(this.projectForm.dirty || !this.projectPage.hideCreateProject){
        return this.dialogService.confirm("Are u sure?");
    }
    else{
        return true;
    }
}

}
