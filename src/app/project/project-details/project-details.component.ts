import { Component, OnInit, OnDestroy, OnChanges, Input, Output, EventEmitter, SimpleChanges, LOCALE_ID} from '@angular/core';
import { Project } from 'src/app/models/project';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/models/user';
import { ProjectService } from 'src/app/services/project.service';
import { ProjectsListComponent } from '../projects-list/projects-list.component';
import { ProjectsPageComponent } from '../projects-page/projects-page.component';

@Component({
  selector: 'project-details',
  templateUrl: './project-details.component.html',
  styleUrls: ['./project-details.component.css',
  '../../../../node_modules/font-awesome/css/font-awesome.css'],
  providers: [ProjectService]
})
export class ProjectDetailsComponent implements OnInit, OnDestroy, OnChanges {

  @Input() item: Project;
  @Input('index') itemIndex: number;
  @Output() itemSelected = new EventEmitter<number>();

  constructor(private projectService: ProjectService,
    private projectsPage: ProjectsPageComponent) { }

  ngOnInit() {
    console.log('ProjectDetailsComponent init');
  }
  ngOnChanges(changes: SimpleChanges): void {
    console.log('ProjectDetailsComponent ngOnChanges', changes);
}

  projectSelected() {
    this.itemSelected.emit(this.itemIndex);
  }

  ngOnDestroy(){
    console.log('ProjectDetailsComponent ngOnDestroy');
  }

  deleteProject() {
    console.log('delete');
    this.projectService.deleteProject(this.item.id).subscribe(x => {
      this.projectsPage.getProjects();
    });

  }

  updateProject() {
    
  }

}
