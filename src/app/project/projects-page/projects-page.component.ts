import { Component, OnInit } from '@angular/core';
import { ProjectService } from 'src/app/services/project.service';
import { Project } from 'src/app/models/project';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { DialogService } from 'src/app/services/dialog.service';

@Component({
  selector: 'app-projects-page',
  templateUrl: './projects-page.component.html',
  styleUrls: ['./projects-page.component.css'],
  providers: [ProjectService]
})
export class ProjectsPageComponent implements OnInit {

  
	hideCreateProject = true;
  projects: Project[] = [];
  selectedProject: Project = {} as Project;
  selectedProjectIndex: number = -1;
  constructor(private projectService: ProjectService, private dialogService: DialogService) { }

  ngOnInit() {
    this.getProjects();
  }

  getProjects () {
    this.projects = [];
    this.projectService.getProjects().pipe(map(x => {
      x = x.map(b => {
          b.deadline = new Date(b.deadline);
          b.created_at = new Date(b.created_at);
          return b;
      })
      return x;
  })).subscribe(x => {
      console.log(x);
      x.forEach(element => {
        this.projects.push(element);
      });
    }, error => {
      console.log(error);
    })
  }

  showEdit() {
    if (this.hideCreateProject) {
      this.selectedProjectIndex = -1;
    } 
    this.selectedProject = {
      name: '',
      description: '',
      deadline: new Date(),
      // created_at: new Date(),
      team_id: null,
      author_id: null
    } as Project;
    this.hideCreateProject = !this.hideCreateProject;
  }

  save(newProject: Project){
    newProject.created_at = new Date(Date.now());
    if(this.selectedProjectIndex === -1){
        this.projects.push(newProject);
        this.projectService.postProject(newProject).subscribe(x => {
          console.log(x);
        });
    } else {
        newProject.id = this.projects[this.selectedProjectIndex].id;
        this.projects[this.selectedProjectIndex] = newProject;
        this.projectService.updateProject(this.projects[this.selectedProjectIndex].id, newProject).subscribe(x => {
          console.log(x);
        });
    }
    this.selectedProjectIndex = -1;
    this.hideCreateProject = true;
    
}

canDeactivate() : boolean | Observable<boolean>{
     
  if(!this.hideCreateProject){
      return this.dialogService.confirm("Are u sure?");
  }
  else{
      return true;
  }
}

select(index: number) {
    let project = this.projects[index];
    this.selectedProject = {
        name: project.name,
        description: project.description,
        deadline: project.deadline,
        author_id: project.author_id,
        team_id: project.team_id

    } as Project;
    this.selectedProjectIndex = index;
    this.hideCreateProject = false;
    console.log(this.selectedProject)
}

}
