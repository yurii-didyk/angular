import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProjectsPageComponent } from './project/projects-page/projects-page.component';
import { TasksPageComponent } from './tasks/tasks-page/tasks-page.component';
import { TeamsPageComponent } from './teams/teams-page/teams-page.component';
import { UsersPageComponent } from './users/users-page/users-page.component';
import { CanDeactivateGuard } from './guards/can-leave-form.guard';
import { ProjectCreateComponent } from './project/project-create/project-create.component';


const routes: Routes = [
  { path: "projects", component: ProjectsPageComponent, canDeactivate: [CanDeactivateGuard]},
  { path: "tasks", component: TasksPageComponent, canDeactivate: [CanDeactivateGuard]},
  { path: "teams", component: TeamsPageComponent, canDeactivate: [CanDeactivateGuard]},
  { path: "users", component: UsersPageComponent, canDeactivate: [CanDeactivateGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [CanDeactivateGuard]
})
export class AppRoutingModule { }
