import { Pipe, PipeTransform } from '@angular/core';
import {DatePipe as AngularDatePipe} from '@angular/common';


@Pipe({
  name: 'datePipe'
})
export class DatePipe implements PipeTransform {

  transform(value: string): any {
    let datePipe = new AngularDatePipe('ua');
    
    return datePipe.transform(value, 'longDate');
  }

}
