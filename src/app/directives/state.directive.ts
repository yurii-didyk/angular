import { Directive, ElementRef, Input, HostListener } from '@angular/core';
import { stringify } from '@angular/compiler/src/util';
import { timeout } from 'rxjs/operators';

@Directive({
  selector: '[state]'
})
export class StateDirective {
  constructor(private elementRef: ElementRef) {
    elementRef.nativeElement.style.fontWeight = 'bold';
  }
  @HostListener('mouseenter') onMouseEnter() {

    const text = this.elementRef.nativeElement.innerText;
    switch(text) {
      case 'Finished':
        this.elementRef.nativeElement.style.color = 'green';
        break;
      case 'Created':
        this.elementRef.nativeElement.style.color = 'blue';
        break;
      case 'Canceled':
        this.elementRef.nativeElement.style.color = 'red';
        break;
      case 'Started':
        this.elementRef.nativeElement.style.color = 'orange';
        break;
    }
  }

}

